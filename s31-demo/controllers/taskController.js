const Task = require('../models/task')

// get all tasks
module.exports.getAllTasks = () => {
  return Task.find({}).then((result) => {
    return result
  })
}

module.exports.createTask = (requestBody) => {
  let newTask = new Task({
    name: requestBody.name
  })
  return newTask.save().then((task, error) => {
    return error ? console.log(error) : task
  })
}

// get task
// get the id from the url
// use that id to find it in the taskCollection then
// use Task model
// Task.findById()
module.exports.getTask = (taskId) => {
  return Task.findById(taskId).then((result, error) => {
    return error ? console.log(error) : result
    // if (error) {
    //   console.log(error)
    //   return false
    // } else {
    //   return result
    // }
  })
}

module.exports.updateTask = (id, newContent) => {
  console.log(newContent)
  return Task.findById(id).then((result, error) => {
    console.log(result)

    if (result == null) return `Document does not exist`
    if (error) {
      console.log(error)
      return false
    }

    result.name = newContent.name
    result.save().then((updatedName, error) => {
      return error ? console.log(error) : updatedName
    })
  })
}

module.exports.deleteTask = (taskId) => {
  return Task.findByIdAndRemove(taskId).then((removeTask, error) => {
    return error ? console.log(error) : removeTask
  })
}
