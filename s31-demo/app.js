const express = require('express')
const mongoose = require('mongoose')

// allows us to use all the routes defined in 'taskRoute.js'
const taskRoute = require('./routes/taskRoute')

const app = express()
const port = 3001

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

mongoose.connect(
  'mongodb+srv://admin:admin1234@zuitt-bootcamp.wvvcs.mongodb.net/s31-demo?retryWrites=true&w=majority',
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
)

// allows all the tasks routes created in the 'taskRoute.js' file
// to use '/tasks' route
app.use('/tasks', taskRoute)

app.listen(port, () => {
  console.log(`Server listening at port ${port} 💪😎🤳`)
})
