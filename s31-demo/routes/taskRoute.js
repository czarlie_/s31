const express = require('express')
const router = express.Router()
const taskController = require('../controllers/taskController')

// route to get all tasks
router.get('/', (req, res) => {
  taskController
    .getAllTasks()
    .then((resultFromController) => res.send(resultFromController))
})

// route to post or add a task
router.post('/', (request, response) => {
  taskController
    .createTask(request.body)
    .then((resultFromController) => response.send(resultFromController))
})

// tasks/69
router.get('/:id', (req, res) => {
  taskController
    .getTask(req.params.id)
    .then((resultFromController) => res.send(resultFromController))
})

// Update a Specific Task
//  http://localhost:3001/
router.put('/:id', (req, res) => {
  console.log(req.params)
  console.log(req.body)
  console.log(req.params.id)
  let id = req.params.id
  let newContent = req.body

  taskController.updateTask(id, newContent).then((result, error) => {
    return error ? res.send(error) : res.send(result)
  })
})

// delete task(s)
router.delete('/delete/:id', (req, res) => {
  taskController.deleteTask(req.params.id).then((resultFromController) => {
    res.send(resultFromController)
  })
})

// use 'module.exports' to export the router object
// to use in the 'app.js'
module.exports = router
